import vk_api
import time 

#Авторизация в вк
LOGIN = ""
PASSWORD = ""
vk_session = vk_api.VkApi(LOGIN,PASSWORD)
vk_session.auth()

#Получаем список фотографии со стены группы с определенным отступом
def getPhoto(userID,owID,count,offset):
	photoIDS = []
	listPhoto = vk_session.method('photos.get',{'owner_id':owID,'album_id':"wall","rev":"1",'count':count,'offset':offset})
	for each in listPhoto['items']:
		photoIDS.append(each['id'])
	return photoIDS

#Генерация ссылок для отправки
def genLinks(userID,owID,count,offset):
	#Получение списка фотографии
	ids = getPhoto(userID,owID,count,offset)
	tmpStrAt = []
	stri = ''
	count = int(count)
	for j in range(count//10+1):
		if count>=10:
			for i in range(10):
				stri +="photo"+str(owID)+"_"+str(ids[i+j*10])+","
		else:
			for i in range(int(count)%10):
				stri +="photo"+str(owID)+"_"+str(ids[i+j*10])+","
		tmpStrAt.append(stri)
		count-=10
		stri = ""
	return tmpStrAt

#Отправка фотографии
def messageSend(userID,owID,count,offset):
	#Получение списка ссылок
	strAT = genLinks(userID,owID,count,offset)
	count = int(count)
	for i in range(count//10+1):
		lol = strAT[i][0:-1]
		try:
			vk_session.method('messages.send',{'user_id':userID,"message":"",'attachment':lol})
			time.sleep(2)
		except:
			pass

#Переписывает куки 
def writeCookie(a):
	k = open('ids.txt','w')
	for b in a:
		stri = ""
		for i in b:
			stri += i+":"+str(b[i])+"|"

		stri = stri[0:-1]
		stri = stri.replace("[",'').replace(']','').replace("'",'').replace(' ','')
		k.write(stri+'\n')
	k.close()

def readall(b):
	#Убирание переноса строки
	for i in range(len(b)):
		b[i] = b[i].strip()
	#Разбивка строк на массивы строк
	for i in range(len(b)):
		b[i] = b[i].split('|')
	return b

def convertToDict(b):
	j  = {}
	ids = []
	for i in b:
		for l in i:
			#Разделение значении по :
			mas = l.split(":")
			#Присваивание ключу словаря определенное значение массива
			j[mas[0]] = mas[1]		
		ids.append(j)
		j = {}
	#Присваивание group_id и offset массива 
	for i in ids:
		i['group_id'] = i['group_id'].split(',')
		i['offset'] = i['offset'].split(',')
	return ids

y = open('ids.txt','r').readlines()	
#Получение списка словарей
ids = convertToDict(readall(y))

def cookie(userID,groupID,count):

	checker = False #Проверяет наличие польльзователя, а затем наличие данной группы у него
	#Цикл проверки
	for i in ids:
		if ( i['user_id'] == str(userID) ) and ( str(groupID) in i['group_id'] ):
			checker = True
			OFFSET = int(i['offset'][i['group_id'].index(str(groupID))])
			#Если пользователь находится в списке и у него есть данная группа, то изменяет переменную checker и отправляет фотографии
			#Отправка
			messageSend(userID,groupID,count,OFFSET)


			#Фотографии
			OFFSET+=int(count)
			i['offset'][i['group_id'].index(str(groupID))] = str(OFFSET)
			break
	#Если данной группы нет у пользователя то добавляет ее в основной список
	if checker == False:
		for i in ids:
			if i['user_id'] == str(userID):
				#Добавлет новую группу и оффсет для нее
				i['group_id'].append(groupID)
				i['offset'].append(0)
				#Еще раз проверяет (немного рекурсии)
				cookie(userID,groupID,count)
	#Переписывает куки
	writeCookie(ids)