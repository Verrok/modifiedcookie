
import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import re
import send

#Авторизация вк
LOGIN = ""
PASSWORD = ""
vk_session = vk_api.VkApi(LOGIN,PASSWORD)

#Класс команд
class commands:
	keys = []
	reply = ""

#Класс обычного ответа на сообщение
class sends(commands):

	def do(self,to):
		values = {"user_id":to,'message':self.reply}
		vk_session.method('messages.send',values)

#Отвечает на приветствие
hello = sends()
hello.keys = ['привет','qq','q','ку']
hello.reply = 'darova'

#Отвечает на как дела и подобное
how = sends()
how.keys = ['как дела','как ты','как дела?','как ты?']
how.reply = "Все хорошо, а у тебя как?"

#Отвечает за отправку ID группы
group = sends()

def main():
	#Пытаемся авторизироваться
	try:
		vk_session.auth()
	except vk_api.AuthError as error_msg:
		print(error_msg)
	#Лонгпол
	longpoll = VkLongPoll(vk_session)
	for event in longpoll.listen():

		if event.type == VkEventType.MESSAGE_NEW:
			#Перевод сообщения в нижний регистр
			mess = event.text.lower()

			#От кого сообщение?
			if event.user_id in [228688466,187759293,152914764,321380104]:
				#Ответ на приветствие
				if mess.split(" ")[0] in hello.keys:
					hello.do(event.user_id)
				#Ответ на "Как дела?" и т.д.
				elif mess in how.keys:
					how.do(event.user_id)
				#Отправка артов
				elif re.search('^ким\s+(арты|артики|арт)\s+-\d+\s+\d+',mess):
					count = mess.split(" ")[-1]
					groupID = re.search("-\d+",mess).group(0)
					send.cookie(event.user_id,groupID,count)
				#Узнать ID группы
				elif re.search("^ким\s+узнай\s+http\w+",mess):
					GROUP_ID = vk_session.method('groups.getById',{'group_id': mess.split("/")[-1]})
					GROUP_ID = GROUP_ID[0]['id']
					group.reply = "-"+ str(GROUP_ID)
					group.do(event.user_id)

				elif re.search("^ким\s+добавь",mess):
					USER_ID = event.user_id
					a = open("ids.txt",'a')
					stri = "user_id:"+str(USER_ID)+"|group_id:0|offset:0"
					a.write(stri+"\n")
					a.close()
					exit()

if __name__ == "__main__":
    main()